﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

using VuelosApp.Models;
using VuelosApp.Models.ViewModels;

namespace VuelosApp.Controllers
{
    public class VuelosController : Controller
    {
        public IActionResult Index()
        {
            MultiTypeFlightsViewModel viewModel = new MultiTypeFlightsViewModel();
            viewModel.NationalFlights = DataMock.Flights.Where(f => f.FlightType.Equals(FlightType.National)).ToList();
            viewModel.NationalCities = City.NationalCities.ToList();

            viewModel.InternationalFlights = DataMock.Flights.Where(f => f.FlightType.Equals(FlightType.International)).ToList();
            viewModel.InternactionalCities = City.InternationalCities.ToList();
            return View(viewModel);
        }

        public IActionResult Nacionales()
        {
            FlightsViewModel viewModel = new FlightsViewModel();
            viewModel.Flights = DataMock.Flights.Where(f => f.FlightType.Equals(FlightType.National)).ToList();
            viewModel.Cities = City.NationalCities.ToList();
            ViewData["titulo"] = "Historial de vuelos nacionales";
            ViewData["origen"] = FlightType.National;
            return View(viewModel);
        }

        public IActionResult Internacionales()
        {
            FlightsViewModel viewModel = new FlightsViewModel();
            viewModel.Flights = DataMock.Flights.Where(f => f.FlightType.Equals(FlightType.International)).ToList();
            viewModel.Cities = City.InternationalCities.ToList();
            ViewData["titulo"] = "Historial de vuelos internacionales";
            ViewData["origen"] = FlightType.International;
            return View("Nacionales", viewModel);
        }

        public IActionResult FindFlights(string origin, string destination, string flighType)
        {
            FlightsViewModel viewModel = new FlightsViewModel();
            if (flighType.Equals(FlightType.National))
            {
                viewModel.Flights = DataMock.Flights.Where(f => f.FlightType.Equals(FlightType.National) && f.Origin.Equals(origin) &&
                                                            f.Destination.Equals(destination)).ToList();
                viewModel.Cities = City.NationalCities.ToList();
                ViewData["titulo"] = "Historial de vuelos nacionales";
                ViewData["origen"] = FlightType.National;
            }
            else
            {
                viewModel.Flights = DataMock.Flights.Where(f => f.FlightType.Equals(FlightType.International) && f.Origin.Equals(origin) &&
                                                            f.Destination.Equals(destination)).ToList();
                viewModel.Cities = City.InternationalCities.ToList();
                ViewData["titulo"] = "Historial de vuelos internacionales";
                ViewData["origen"] = FlightType.International;
            }

            return View("Nacionales", viewModel);
        }

        public IActionResult Create()
        {
            CreateFlightViewModel model = new CreateFlightViewModel();

            model.Cities = City.NationalCities.ToList();

            model.States = new List<string>() { State.Waiting, State.OnWay, State.OnBoarding,
                State.Arrival, State.Closed };

            return View(model);
        }

        public IActionResult Save(Flight flight)
        {
            DataMock.Flights.Add(flight);
			if (flight.FlightType.Equals(FlightType.National))
			{
                return RedirectToAction(nameof(Nacionales));
			}
			else
			{
                return RedirectToAction(nameof(Internacionales));
            }
            
        }

        public IActionResult GetCities(string flightType)
		{
            List<string> cities = new List<string>();

			if (flightType.Equals(FlightType.National))
			{
                cities.AddRange(City.NationalCities.ToList());
			}
			else
			{
                cities.AddRange(City.InternationalCities.ToList());
            }

            return Json(cities);
		}
    }
}
