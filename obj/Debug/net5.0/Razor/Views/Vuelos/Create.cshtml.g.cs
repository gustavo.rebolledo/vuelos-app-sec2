#pragma checksum "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1fe6eada11b1bba075f996faaf173a8de0ecc619"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Vuelos_Create), @"mvc.1.0.view", @"/Views/Vuelos/Create.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\_ViewImports.cshtml"
using VuelosApp;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\_ViewImports.cshtml"
using VuelosApp.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1fe6eada11b1bba075f996faaf173a8de0ecc619", @"/Views/Vuelos/Create.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"83e2f2b0e4f750cc4db77b85a16720ffd56a8312", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Vuelos_Create : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<VuelosApp.Models.ViewModels.CreateFlightViewModel>
    #nullable disable
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("/vuelos/save"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
  
	ViewData["Title"] = "Nuevo vuelo";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            WriteLiteral("\r\n<div class=\"container-fluid\">\r\n\t<h2 class=\"text-center\">");
#nullable restore
#line 8 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
                       Write(ViewData["Title"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h2>\r\n\t");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1fe6eada11b1bba075f996faaf173a8de0ecc6194685", async() => {
                WriteLiteral(@"
		<div class=""form-group"">
			<label for=""date"">Fecha</label>
			<input name=""date"" class=""form-control"" placeholder=""10/05/2021 13:15"" required />
		</div>
		<div class=""form-group"">
			<label>Tipo de vuelo</label>
			<div class=""form-check"">
				<label class=""form-check-label"">
					<input name=""flightType"" class=""form-check-input"" type=""radio""");
                BeginWriteAttribute("value", " value=\"", 588, "\"", 616, 1);
#nullable restore
#line 18 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
WriteAttributeValue("", 596, FlightType.National, 596, 20, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" required />\r\n\t\t\t\t\tNacional\r\n\t\t\t\t</label>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"form-check\">\r\n\t\t\t\t<label class=\"form-check-label\">\r\n\t\t\t\t\t<input");
                BeginWriteAttribute("id", " id=\"", 749, "\"", 754, 0);
                EndWriteAttribute();
                WriteLiteral(" class=\"form-check-input\" name=\"flightType\" type=\"radio\"");
                BeginWriteAttribute("value", " value=\"", 811, "\"", 844, 1);
#nullable restore
#line 24 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
WriteAttributeValue("", 819, FlightType.International, 819, 25, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" required />\r\n\t\t\t\t\tInternacional\r\n\t\t\t\t</label>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"form-group\">\r\n\t\t\t<label for=\"origin\">Ciudad de origen:</label>\r\n\t\t\t<select name=\"origin\" class=\"form-control\" required>\r\n");
#nullable restore
#line 32 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
                 foreach (string city in Model.Cities)
				{

#line default
#line hidden
#nullable disable
                WriteLiteral("\t\t\t\t\t");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1fe6eada11b1bba075f996faaf173a8de0ecc6197013", async() => {
#nullable restore
#line 34 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
                       Write(city);

#line default
#line hidden
#nullable disable
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
#nullable restore
#line 35 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
				}

#line default
#line hidden
#nullable disable
                WriteLiteral("\t\t\t</select>\r\n\t\t</div>\r\n\t\t<div class=\"form-group\">\r\n\t\t\t<label for=\"destination\">Ciudad de destino:</label>\r\n\t\t\t<select name=\"destination\" class=\"form-control\" required>\r\n");
#nullable restore
#line 41 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
                 foreach (string city in Model.Cities)
				{

#line default
#line hidden
#nullable disable
                WriteLiteral("\t\t\t\t\t");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1fe6eada11b1bba075f996faaf173a8de0ecc6198880", async() => {
#nullable restore
#line 43 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
                       Write(city);

#line default
#line hidden
#nullable disable
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
#nullable restore
#line 44 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
				}

#line default
#line hidden
#nullable disable
                WriteLiteral(@"			</select>
		</div>
		<div class=""form-group"">
			<label for=""numFlight"">Número de vuelo</label>
			<input name=""numFlight"" class=""form-control"" placeholder=""AC5825"" required />
		</div>
		<div class=""form-group"">
			<label for=""state"">Estado del vuelo:</label>
			<select name=""state"" class=""form-control"" required>
");
#nullable restore
#line 54 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
                 foreach (string state in Model.States)
				{

#line default
#line hidden
#nullable disable
                WriteLiteral("\t\t\t\t\t");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1fe6eada11b1bba075f996faaf173a8de0ecc61910894", async() => {
#nullable restore
#line 56 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
                       Write(state);

#line default
#line hidden
#nullable disable
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
#nullable restore
#line 57 "C:\Users\grebo\Documents\ipvg\codigos\seccion 2\mvc-vuelosapp\Views\Vuelos\Create.cshtml"
				}

#line default
#line hidden
#nullable disable
                WriteLiteral("\t\t\t</select>\r\n\t\t</div>\r\n\t\t<button type=\"submit\" class=\"btn btn-primary float-right\">Guardar</button>\r\n\t");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</div>\r\n\r\n\r\n");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<VuelosApp.Models.ViewModels.CreateFlightViewModel> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
