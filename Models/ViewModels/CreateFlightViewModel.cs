﻿using System.Collections.Generic;

namespace VuelosApp.Models.ViewModels
{
    public class CreateFlightViewModel
    {
        public List<string> Cities { get; set; }

        public List<string> States { get; set; }
    }
}
